import pytest
from django.urls import reverse

from onlinegrocery.models import PageViewCount


def test_goodbye_view(client):
    url = reverse("goodbye")
    response = client.get(url)
    assert response.status_code == 200
    assert response.content == b"Goodbye world"
