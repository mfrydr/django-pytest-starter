from django.apps import AppConfig


class OnlinegroceryConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'onlinegrocery'
